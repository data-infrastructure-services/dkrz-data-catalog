jupyter nbconvert --to python .clear_and_prepare_for_ci.ipynb
ipython .clear_and_prepare_for_ci.py
for dir in dashboards scripts; do
	rm $dir/index.md
	for n in $(ls $dir -1); do
		echo "- $n" >> $dir/index.md
	done;
done;
jupyter-book toc from-project . -e .ipynb -e .md -t > _toc.yml
jupyter-book build .
