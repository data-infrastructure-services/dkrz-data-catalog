import intake
import yaml
import requests
import pytest

@pytest.fixture
def added_file_content(request):
    # Get the path of the added file in the GitLab repository
    added_file_path = request.config.getoption('--added-file')

    # Read the content of the added file
    with open(added_file_path, 'r') as f:
        content = f.read().rstrip()

    return content
    
def test_can_open_url_with_intake(added_file_content):
    # Check if the URL can be opened with intake
    assert yaml.safe_load(
        requests.get(added_file_content).content
    ), "Could not parse contents of the file with yaml"
    catalog = intake.open_catalog(added_file_content)
    assert isinstance(catalog, intake.catalog.local.YAMLFileCatalog), \
        f"Expected catalog to be a YAMLFileCatalog, but got {type(catalog)}"
    
def test_metadata_specifications(added_file_content):
    # Check if the metadata section contains specifications for 'account' and 'part_of'
    catalog = intake.open_catalog(added_file_content)
    assert catalog.metadata, "No metadata section found"

    account_spec = catalog.metadata.get('account')
    part_of_spec = catalog.metadata.get('part_of')

    assert account_spec or part_of_spec, "No specifications found for 'account' or 'part_of'"
    
    assert isinstance(account_spec, (str, list)), "'account' specification should be a string or a list of strings"
    assert isinstance(part_of_spec, (str, list)), "'part_of' specification should be a string or a list of strings"

def test_at_least_one_entry_can_be_opened(added_file_content):
    catalog = intake.open_catalog(added_file_content)
    # Check if there is at least one entry that can be opened with intake
    entries = list(catalog)
    assert entries, "No entries found in the file"

    l_loadable=False
    for entry in entries:
        try:
            entry_catalog = catalog[entry]
            entry_catalog.describe()
            l_loadable=True
            break
        except:
            continue
    assert l_loadable, f"Failed to open the first entry with intake. URL: {entry_url}"
