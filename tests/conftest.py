import pytest

def pytest_addoption(parser):
    parser.addoption("--added-file", action="store", default=None, help="Path to the added file in the GitLab repository")