# Contributing to DKRZ's data lake with a catalog

## Requirements

### Maintained externally

- You need to be "maintainer" of the DKRZ gitlab repository which you want to link

### Maintained internally

- You need to become "maintainer" of this repository to maintain your catalog

# Receipe

1. Decide wether you want to maintain the catalog externally or internally

2. Add a file `YOURFILE` to `/lake/sources/YOURFILE`

**Externally maintained**:

- The file contains only the link to the raw version of the proposed catalog. Find and example [here]().

**Internally maintained**:

- The file contains entries of only two types:
    - Loadable data sources
    - Links to catalogs within DKRZ's gitlab 
